import { Box, Button, Center, Container, FormControl, FormErrorMessage, FormLabel, HStack, Input } from '@chakra-ui/react'
import { useAuth } from 'hooks'
import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useLocation, useNavigate } from 'react-router-dom'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { onAuthStateChanged } from 'firebase/auth'
import { auth } from 'config'

type LoginFromInput = {
    email: string
    password: string
  }
  
  interface LocationState {
    from: {
      pathname: string
    }
  }

const schema = yup.object().shape({
    email: yup.string().required('Please enter a valid email address'),
    password: yup.string().required('Please enter a valid password')
})
function LoginPage() {
    const navigate = useNavigate()
    const location = useLocation()
    const { login, setLoading } = useAuth()
    const {handleSubmit, register, formState: { errors, isSubmitting}} = useForm<LoginFromInput>({
        resolver:yupResolver(schema)    
    })
    
    const { from } = (location?.state as LocationState) || {
      from: { pathname: '/' },
    }

    async function onSubmit(value:LoginFromInput){
        const { email, password } = value
        await login(email, password)
        await navigate(from, { replace: true })
    }

    useEffect(() => {
        onAuthStateChanged(auth, user => {
          if (user?.uid) {
            navigate('/')
            setLoading(false)
          } else {
            setLoading(false)
          }
        })
        // eslint-disable-next-line
      }, [])
    
  return (
    <Box h={'100vh'}>
            <Center h={'full'}>
                <Container >
                <form onSubmit={handleSubmit(onSubmit)}>
                    <FormControl isInvalid={!!errors.email}>
                        <FormLabel>Email</FormLabel>
                        <Input type={"text"} {...register('email',{required:'Please enter a valid email'})} />
                        <FormErrorMessage>{errors.email && errors.email.message}</FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!errors.password}>
                        <FormLabel>Password</FormLabel>
                        <Input type={"password"} {...register('password',{required:'Please enter a valid password'})} />
                        <FormErrorMessage>{errors.password && errors.password.message}</FormErrorMessage>
                    </FormControl>
                    <HStack justifyContent={'end'}>
                    <Button my={'.75em'} colorScheme={'linkedin'} type="submit" isLoading={isSubmitting}>Submit</Button>
                    </HStack>
                </form>
                </Container>
            </Center>
    </Box>
  )
}

export default LoginPage