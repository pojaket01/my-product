import { Box, Button, Container, Fade, useColorMode} from '@chakra-ui/react'
import { useProduct } from 'hooks'
import React, { useState } from 'react'
import { useEffect } from 'react'

export default function HomePage({user}:{user:any}) {
    const [ isLoading, setIsLoading ] = useState<boolean>(true)
    const { colorMode ,toggleColorMode } = useColorMode()
    const { getAllProduct, Product, getAllProductStock } = useProduct()

    // const fetchData = useCallback(async() => {
    //     const res = await getAllProduct()
    //     setProduct(res)
    // },[])
    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false)
        },1000)
        setTimeout(() => {
            getAllProduct()
            getAllProductStock()
        },1500)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])
// console.log(Product)
    return (
    <Box pt={'5'}>
        <Container maxW={{base:'container.sm', md:'container.md', lg:'container.lg'}}>
            <Fade in={!isLoading} >
                <Box p={'5'} borderRadius={'1.2em'} bg={colorMode === "dark" ? 'whiteAlpha.100': 'blackAlpha.100'} h={'lg'} w={'full'}>
                    <Button onClick={toggleColorMode}>Click</Button>
                </Box>
            </Fade>
        </Container>
    </Box>
  )
}