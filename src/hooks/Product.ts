import { useState, useEffect } from "react"
import { onSnapshot, doc, collection, addDoc, updateDoc, deleteDoc, query} from 'firebase/firestore'
import { db } from "config"
export function useProduct() {
    const [ Product, setProduct ] = useState<any | []>([])
    const ProductRef = collection(db,"Products")

    // const ProductDoc = doc(db,"Products", Product?.id)
    // const ProductStockRef = collection(ProductDoc,"stock")

    // eslint-disable-next-line react-hooks/exhaustive-deps
    async function getAllProduct() {
        const res = onSnapshot(ProductRef,(Snapshot) => {
            return setProduct(Snapshot.docs.map((doc) => ({
                id: doc?.id,
                ...doc.data()
            })))
        })
        return res
    }
    
    function getAllProductStock() {
        Product.map((itm:any) => {
            const ProductDoc = doc(db, "Products", itm?.id)
            const ProductCollect = collection(ProductDoc,'stock')
            const ProductQuery = query(ProductCollect)
            
            onSnapshot(ProductQuery, (Snapshot) => console.log(Snapshot.docs.map((doc) => ({id:doc?.id, ...doc.data()}))))
        })
    }

    function getProductId(id:string){

    }

    function getProductIdStock(id:string){
        const ProductDoc = doc(db, "Products", id)
            const ProductCollect = collection(ProductDoc,'stock')
            const ProductQuery = query(ProductCollect)
        
            return onSnapshot(ProductQuery, (Snapshot) => console.log(Snapshot.docs.map((doc) => ({id:doc?.id, ...doc.data()}))))
        // const ProductStockRef = collection(ProductDoc,"stock")
        // console.log(ProductStockRef)

    }

    function createProduct(value:any) {

    }

    function updateProduct(value:any) {

    }

    function updateProductStock(value:any) {

    }

    function removeProduct(id:string) {

    }

    function removeProductStock(id:string) {

    }

    useEffect(() => {
        
    },[])

    return { 
        createProduct, 
        updateProduct, 
        updateProductStock, 
        removeProduct, 
        removeProductStock, 
        getAllProduct, 
        getAllProductStock,
        getProductId,
        getProductIdStock,
        Product 
    }
}