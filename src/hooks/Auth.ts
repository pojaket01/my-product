import { auth } from 'config'
import { useAppContext } from 'context/AppContext'
import { onAuthStateChanged, signInWithEmailAndPassword, signOut } from 'firebase/auth'
import { useEffect, useState } from 'react'

export function useAuth() {
    const [ user, setUser ] = useState<any | null>()
    const { setError, setMessage } = useAppContext()
    const [isLoading, setLoading] = useState(true)

    useEffect(() => {
        onAuthStateChanged(auth, async(authUser) => {
            if (authUser?.uid) {
                let idToken = await authUser.getIdTokenResult()
                let claims = idToken?.claims
                setUser({
                    ...authUser,
                    role:claims.role
                })
                setTimeout(() => {
                    setLoading(false)
                },3000)     
                return user   
                
              } else {
                setUser(null)
                setLoading(false)        
              }
          
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])

    async function login(email: string, password: string) {
        await signInWithEmailAndPassword(auth, email, password).then(async({ user:User }) => {
            if(User) {
                setUser(User)
                setMessage({type:'success', message:'Login Successfully'})
                return user
            }
        }).catch((error:any) => {
            setError({code:'Login Error', message:error?.message})
        })
    }

    async function logout(){
        try {
            await signOut(auth).then(() => {
                setMessage({type:'success', message:'Logout Successfully'})
            })
        } catch (errors:any) {
            setError({code:'Logout Error', message:errors})
        }
    }

    return { login, logout, user, setUser, isLoading, setLoading }
}