import HomePage from "pages/Home"
import { useAuth } from "hooks/Auth"
import { IRoute } from "interfaces"
import LoginPage from "pages/Login"
import { Navigate, useLocation } from "react-router-dom"
import { MainLayout } from "layouts"

function RequireAuth({ children, user }: { children: JSX.Element, user: any }) {
    const location = useLocation()
    if (!user) {
      return <Navigate to='/login' state={{ from: location }} replace />
    }

    return children
    
  }
  

function useStoreRoute() {
    const { user } = useAuth()

    const routes: IRoute[] = [
        {
            path:'/',
            element: (
                <RequireAuth user={user}>
                    <MainLayout>
                        <HomePage user={user} />
                    </MainLayout>
                </RequireAuth>
            )
        },
        {
            path:'/login',
            element: (
                <LoginPage />
            )
        }
    ]
    return { routes }
}
  
export default useStoreRoute
  