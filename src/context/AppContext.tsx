import { useState, useContext, createContext } from 'react'

import { IAppContext, Error, Message } from 'types/common'

const AppContext = createContext<IAppContext>({
  user: null,
})

const ContextProvider = ({ children }: { children: JSX.Element }) => {
  const [error, setError] = useState<Error>()
  const [message, setMessage] = useState<Message>()

  const { Provider } = AppContext

  const value = {
    error,
    setError,
    message,
    setMessage,
  }

  return <Provider value={value}>{children}</Provider>
}

const useAppContext = () => useContext(AppContext)

export { ContextProvider, AppContext, useAppContext }
