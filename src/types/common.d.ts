export type Error = {
    code: string
    message: string
}

export type Message = {
    type: Color
    message: string
}

export interface IAppContext {
    error?: Error | undefined
    setError?: Dispatch<SetStateAction<Error | undefined>>
    message?: Message | undefined
    setMessage?: Dispatch<SetStateAction<Message | undefined>>
    user?: TUser | null
    setUser?: Dispatch<SetStateAction<TUser | null | undefined>>
}