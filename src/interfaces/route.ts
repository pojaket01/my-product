export interface IRoute {
    path: string,
    element: JSX.Element,
    props?: any
}