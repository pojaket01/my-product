import { initializeApp } from 'firebase/app'
import { getFunctions, connectFunctionsEmulator } from 'firebase/functions'
import { getAuth, connectAuthEmulator } from 'firebase/auth'
import { getFirestore, connectFirestoreEmulator } from 'firebase/firestore'
import { getStorage, connectStorageEmulator } from 'firebase/storage'
const config = {
	// apiKey: "AIzaSyDg6lGCIubMfx2TR8fr9eTfTsTI9WajD4w",
	// authDomain: "movmall-dev.firebaseapp.com",
	// databaseURL: "https://movmall-dev.firebaseio.com",
	// projectId: "movmall-dev",
	// storageBucket: "movmall-dev.appspot.com",
	// messagingSenderId: "114785628824"
    apiKey: "AIzaSyAQ_8ezIz9h1hJqqeu1u7jiL8Rqpu053YA",
    authDomain: "test-product-ffc51.firebaseapp.com",
    projectId: "test-product-ffc51",
    storageBucket: "test-product-ffc51.appspot.com",
    messagingSenderId: "208973152668",
    appId: "1:208973152668:web:6f37237c59bde76bce0859",
    measurementId: "G-3VWK9VXEL6"
}

initializeApp(config)

const functions = getFunctions()
const auth = getAuth()
const db = getFirestore()
const storage = getStorage()

if (window.location.hostname === 'localhost') {
	connectAuthEmulator(auth, "http://localhost:9099")
    connectFunctionsEmulator(functions, "localhost", 5001)
	connectFirestoreEmulator(db, "localhost", 8080)
	connectStorageEmulator(storage, "localhost",9199)
}


export { functions, auth, db, storage }