import { NavBar } from 'components'
import React from 'react'

export function MainLayout({children}: { children: JSX.Element}) {
  return (
    <>
        <NavBar/>
        {children}
    </>
  )
}
