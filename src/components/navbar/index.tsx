import {
  Box,
  Button,
  Heading,
  HStack,
  Icon,
  Menu,
  MenuButton,
  MenuDivider,
  MenuGroup,
  MenuItem,
  MenuList,
  useColorMode,
} from '@chakra-ui/react'
import { useAuth } from 'hooks/Auth'
import { FiLogOut } from 'react-icons/fi'
import { IoMdPerson } from 'react-icons/io'
import React from 'react'

export function NavBar() {
  const { logout } = useAuth()
  const { colorMode } = useColorMode()

  return (
    <HStack
      bg={colorMode === 'dark' ? '#1e232e' : 'whiteAlpha.700'}
      p={'.75em'}
      w={'full'}
      justifyContent={'space-between'}
      boxShadow={'lg'}
    >
      <Heading color={colorMode === 'dark' ? 'white' : 'black'}>Navigation</Heading>
      <Box>
        <Menu>
          <MenuButton as={Button} variant={'none'} _hover={{bg:'#EAEAEA', color:'black'}} color={colorMode === 'dark' ? 'white' : 'black'}>
            <Icon fontSize={'24px'} textAlign={'center'}><IoMdPerson/></Icon>
          </MenuButton>
          <MenuList color={colorMode === 'dark' ? 'white' : 'black'}>
            <MenuGroup title='Profile'>
              <MenuItem>My Account</MenuItem>
              <MenuItem>Payments </MenuItem>
            </MenuGroup>
            <MenuDivider />
            <MenuGroup title='Setting'>
              <MenuItem>Docs</MenuItem>
              <MenuItem>FAQ</MenuItem>
            </MenuGroup>
            <MenuDivider />
            <MenuGroup>
              <MenuItem justifyContent={'center'} onClick={() => logout()}>
                <Icon fontSize={'24px'} textAlign={'center'}><FiLogOut /></Icon>
              </MenuItem>
            </MenuGroup>
          </MenuList>
        </Menu>
      </Box>
    </HStack>
  )
}
